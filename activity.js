
let button = document.querySelector("#submit-btn");

let inputFirstName = document.querySelector("#txt-first-name");
let inputLastName = document.querySelector("#txt-last-name");

let fullNameDisplay = document.querySelector("#full-name-display");

const showName = () => {
	console.log(inputFirstName.value); 
	console.log(inputLastName.value); 
	fullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`;
}

inputFirstName.addEventListener('keyup', showName);
inputLastName.addEventListener('keyup', showName);

button.addEventListener('click', () => {

	if (inputFirstName.value === "" || inputLastName.value === ""){
		alert("Please input first name and last name.")
	} else{
		alert(`Thank you for registering, ${fullNameDisplay.innerHTML}`)
		inputFirstName.value = "";
		inputLastName.value = "";
	}

})
